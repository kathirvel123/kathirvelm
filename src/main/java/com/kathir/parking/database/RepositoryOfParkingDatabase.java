package com.kathir.parking.database;
import com.kathir.parking.parkingClass.Parking;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

//Repository of Parking database Curd function

@Repository
public interface RepositoryOfParkingDatabase extends CrudRepository<Parking,Integer>{

}
