package com.kathir.parking.parkingClass;
import javax.persistence.Entity;
import javax.persistence.Id;
//ENTITY Of info database
@Entity
public class Info {
    
    //vehicleNo is primary key
    @Id
    Integer vehicleNo;
    String name;
    Integer id;
    String vehicle;

    public Info() {
    }

   //getter and setter functions
    public Integer getVehicleNo() {
        return vehicleNo;
    }

    public void setVehicleNo(Integer vehicleNo) {
        this.vehicleNo = vehicleNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getVehicle() {
        return vehicle;
    }

    public void setVehicle(String vehicle) {
        this.vehicle = vehicle;
    }
}
